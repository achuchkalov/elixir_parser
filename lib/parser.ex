defmodule Parser do

  # скачивает и обрабатывает страницу
  def start({url, max_depth}, analyze) do
    %{host: host} = URI.parse(url)

    spawn fn ->
      try do
        IO.puts "process #{url}"
        case HTTPoison.get(url, [], [recv_timeout: 1500]) do
          # если скачивание страницы было успешным
          {:ok, %HTTPoison.Response{status_code: 200, body: html}} ->
            if max_depth > 0 do
              # если не достигли максимальной глубины переходов, добавляем в хранилище адресов найденные на странице ссылки
              extract_internal_links_and_add_to_storage(html, host, max_depth - 1)
            end
            # вызываем функцию-анализатор для содержимого. Функция определяется в Scraper.
            analyze.(html)
          # если при скачивании страницы сервер ответил кодом редиректа, добавляем адрес редиректа как новую ссылку
          {:ok, %HTTPoison.Response{status_code: 301, headers: headers}} ->
            add_url_and_notify({extract_redirect_goal_from(headers), max_depth - 1})
          # ничего не делаем при любых других результатах скачивания
          _ -> nil
        end
      after
        # после скачивания (даже неуспешного) уведомляем менеджер о завершении работы
        ParserManager.notify_about_finish_parsing(self)
      end
    end
  end

  defp extract_internal_links_and_add_to_storage(html, host, next_depth) do
    Floki.attribute(html, "a", "href")
      |> Enum.filter(&internal_link/1)
      |> Enum.map(fn path -> host <> path end)
      |> Enum.map(fn x -> add_url_and_notify({x, next_depth}) end)
  end

  # Является ссылка внешней или внутренней. Мы переходим только по внутренним ссылкам.
  defp internal_link(x) when x == "", do: false
  defp internal_link(x), do: Regex.match?(~r/^\/\w/, x)

  defp extract_redirect_goal_from headers do
    {_, redirect_goal} = Enum.find(headers, fn {name, _} -> name == "Location" end)
    redirect_goal
  end

  defp add_url_and_notify({url, depth}) do
    UrlsToFetchStorage.add_url({url, depth})
    IO.puts 'added #{url}'
    ParserManager.notify_about_available_url
  end

end
