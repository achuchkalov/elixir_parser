defmodule ParserManager do
  @max_parsers_amount 25
  @max_depth 2

  # запускает хранилища url и запущенных парсеров, а также запускат процесс парсинга
  # @url - начальный адрес
  # @analyzer - функция для обработки содержимого страницы. В нашем случае извлекает все упоминания слова.
  # @on_finish - функция для запуска после окончания парсинга
  def start(url, analyzer, on_finish) do
    max_depth = @max_depth
    UrlsToFetchStorage.start
    UrlsToFetchStorage.add_url({url, max_depth})
    Agent.start_link(fn -> analyzer end, name: :AnalyzerStorage)
    Agent.start_link(fn -> on_finish end, name: :OnFinishStorage)

    Agent.start_link(fn -> [] end, name: :ParsersStorage)
    notify_about_available_url()
  end

  # парсеры будут вызывать эту функцию при нахождении нового адреса
  def notify_about_available_url do
    run_parsers_if_can
  end

  # Вызывается парсером при окончании обработки страницы.
  # Удаляет переданный парсер из списка, запускает следующий парсер или уведомляет об окончании парсинга
  def notify_about_finish_parsing(parser) do
    Agent.update(:ParsersStorage, fn parsers ->
      parsers -- [parser]
    end)

    if no_running_parsers? && UrlsToFetchStorage.empty? do
      finish_parsing
    else
      run_parsers_if_can
    end
  end

  # Если в хранилище url есть необработанные записи и количество уже запущенных парсеров меньше максимума,
  # запускаем новый парсер
  defp run_parsers_if_can do
    Agent.update(:ParsersStorage, fn parsers ->
      if Enum.count(parsers) < @max_parsers_amount do
        case UrlsToFetchStorage.get_next_url do
          nil -> parsers
          next_url -> [Parser.start(next_url, get_analyzer) | parsers]
        end
      else
        parsers
      end
    end)
  end

  # запускает коллбэк и завершает уже ненужные процессы
  defp finish_parsing do
    get_on_finish.()
    Agent.stop(:AnalyzerStorage)
    Agent.stop(:OnFinishStorage)
    Agent.stop(:ParsersStorage)
    UrlsToFetchStorage.stop
  end

  # достаём функцию-анализатор, определённую в Scraper.
  defp get_analyzer do
    Agent.get(:AnalyzerStorage, fn analyzer -> analyzer end)
  end

  # достаём функцию для запуска по завершении парсиинга, определённую в Scraper.
  defp get_on_finish do
    Agent.get(:OnFinishStorage, fn on_finish -> on_finish end)
  end

  defp no_running_parsers? do
    Agent.get :ParsersStorage, fn parsers ->
      Enum.empty?(parsers)
    end
  end
end