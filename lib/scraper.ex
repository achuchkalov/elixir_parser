defmodule Scraper do

  @tags_with_github_regex  ~r/[^<>]*github[^<>]*/i

  def start do
    Agent.start(fn -> [] end, name: :ScraperStorage)
    ParserManager.start("https://github.com/", &analyzer/1, &on_finish/0) #http://luxexpress.eu/ru  https://github.com/
  end

  # ищет упоминания github в тексте страницы и добавляет найденные фразы в хранилище результатов
  def analyzer(html) do
    github_mentions = Enum.map(Floki.find(html, "a, p, h1, h2, h3, span") , fn x -> Floki.text(x) end) # найти все ссылки, параграфы, заголовки и тексты
     # взять содержащиеся в них тексты
    |> Enum.flat_map(fn x -> Regex.scan(@tags_with_github_regex, x) end) # взять вхождения подстроки github в них
    |> Enum.flat_map(fn x -> x end) # сделать массив "плоским", без вложенных массивов
    |> Enum.map(fn x -> String.strip(x) end) # обрезать пробелы в конце и в начале найденных строк

    Agent.update(:ScraperStorage, fn already_parsed_mentions ->
      github_mentions ++ already_parsed_mentions
    end)
  end

  # записывает найденные результаты в файл по окончании процесса
  def on_finish do
    results = Agent.get(:ScraperStorage, fn x -> x end)
    |> Enum.uniq
    |> Enum.join("!!\n\n")
    File.write("./results.txt", results)
    IO.puts "Finished parsing. Results were written into results.txt."
  end
end