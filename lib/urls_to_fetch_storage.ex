defmodule UrlsToFetchStorage do
  def start do
    Agent.start(fn -> %{to_fetch: [], all: []} end, name: :UrlsToFetchStorage)
  end

  def stop do
    Agent.stop(:UrlsToFetchStorage)
  end

  # Если не обрабатывали переданный адрес, добавляем его в хранилище
  def add_url(item) do
    {url, max_depth} = item
    Agent.update(:UrlsToFetchStorage, fn current_state ->
      if Enum.member?(current_state.all, url) do
        current_state
      else
        new_all = [url | current_state.all]
        new_to_fetch = [item | current_state.to_fetch]
        %{all: new_all, to_fetch: new_to_fetch}
      end
    end)
  end

  # Возвращаем очередной адрес и удаляем его из хранилища
  def get_next_url do
    Agent.get_and_update(:UrlsToFetchStorage, fn state ->
      case state.to_fetch do
        [head | tail] ->
          {head, %{state | to_fetch: tail}}
        _ ->
          {nil, state}
      end
    end)
  end

  def empty? do
    Agent.get(:UrlsToFetchStorage, fn state -> state.to_fetch end)
      |> Enum.empty?
  end
end